# README

Personal configuration for [Vale](https://vale.sh/), the open source linter for prose.

Configuration based on Vale [documentation](https://vale.sh/docs/) and [configuration generator](https://vale.sh/generator).

## How to use

Place the .vale.ini file and the .styles directory in your $HOME and run ```vale sync``` to download all the packages. Run ```vale ls-config``` to confirm that it works. Use ```vale <file>``` to lint your prose.

To add accepted terms, edit ```.styles/Vocab/WikimediaTech/accept.txt```, or create a new directory in Vocab and add it to the configuration.
